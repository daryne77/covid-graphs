import csv
import urllib.request as request
import codecs
import matplotlib.pyplot as plt
import numpy as np


deaths_url = "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_19-covid-Deaths.csv"
confirmed_url = "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_19-covid-Confirmed.csv"
recovered_url = "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_19-covid-Recovered.csv"

deaths_csv = request.urlopen(deaths_url)
deaths_reader = csv.reader(codecs.iterdecode(deaths_csv, 'utf-8'))

confirmed_csv = request.urlopen(confirmed_url)
confirmed_reader = csv.reader(codecs.iterdecode(confirmed_csv, 'utf-8'))

recovered_csv = request.urlopen(recovered_url)
recovered_reader = csv.reader(codecs.iterdecode(recovered_csv, 'utf-8'))

headers = next(deaths_reader, None)
headers = next(confirmed_reader, None)
headers = next(recovered_reader, None)

count = 1

for deaths, confirmed, recovered in zip(deaths_reader, confirmed_reader, recovered_reader):
	if deaths[0] != confirmed[0] or deaths[0] != recovered[0]:
		print("Error, headers don't match")
		break

	if count % 10 == 0:
		print("Calculating item no. " + str(count))

	province_state = deaths[0]
	country_region = deaths[1]
	deaths    = [int(y) for y in deaths[4:]]
	confirmed = [int(y) for y in confirmed[4:]]
	recovered = [int(y) for y in recovered[4:]]
	plt.plot(headers[4:], deaths, label="deaths")
	plt.plot(headers[4:], confirmed, label="confirmed")
	plt.plot(headers[4:], recovered, label="recovered")
	plt.title(province_state + " - " + country_region + " - " + str(max(confirmed)) + " cases")
	left, right = plt.xlim()
	bottom, top = plt.ylim()
	plt.xticks(np.arange(0, right, step=right/7))
	plt.yticks(np.arange(0, top + 1, step=max([1, round(top/10)])))
	plt.legend()
	plt.savefig(province_state + " - " + country_region + ".png")
	plt.close()

	count = count + 1

